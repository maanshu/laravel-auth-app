<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class pagesController extends Controller
{
    // Homepage

    public function index()
    {
        return view('pages.index');
    }
    public function table()
    {
        return view('pages.table');
    }

}
